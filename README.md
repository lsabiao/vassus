# Vassus
(Vassus means Employee in latin)

## Render images in your terminal.

![base](https://i.imgur.com/TKro10h.jpg)

![render](https://i.imgur.com/NbriMtd.png)

## how to use

```shell
python vassus.py path/to/img
```
### eg:
```shell
python vassus.py https://imgur.com/TKro10h
```
or
```shell
python vassus.py example.jpg
```

## Dependencies:
[Pillow](https://python-pillow.org/)


![wtfpl](http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-1.png)
